# daelvn/Dotfiles
Just my normal dotfiles, categorized and put into a folder
## How I organize my dotfiles
All the configurations are stored in ~/Dotfiles and this git repository is organized in three branches:
- `master`: Latest working and usable dotfiles
- `latest`: Latest dotfiles, might be unfinished or untested
- `backup`: Backup of the dotfiles, might contain completely random files

## Categories
- bash: Anything bash related (.bashrc, .bash_profile)
- X: Xorg-server configurations
- i3: My window manager things
- dm: Display manager options (.dmrc)
- Scripts: Any kind of scripts (Color scripts, customs...)

